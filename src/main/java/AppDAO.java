import fr.epsi.b3.jdbc.bo.User;
import fr.epsi.b3.jdbc.dal.DAOFactory;
import fr.epsi.b3.jdbc.dal.PersistenceManager;
import fr.epsi.b3.jdbc.dal.iUserDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class AppDAO {
	
	private static final Scanner keyboard = new Scanner( System.in );
	
	private static void exit() {
		try {
			PersistenceManager.closeConnection();
		} catch ( SQLException e ) {
			System.out.println( "Attention, je n'ai pas pu fermer la connexion" );
		}
	}
	
	private static void createUser() {
		
		iUserDAO dao = DAOFactory.getUserDAO();
		User user = new User( "TDO", "tdo@ss.org" );
		try {
			dao.create( user );
		} catch ( SQLException e ) {
			e.printStackTrace();
		}
		System.out.println( user );
	}
	
	private static void parcours() {
		
		iUserDAO dao = DAOFactory.getUserDAO();
		try ( ResultSet rs = dao.getUsersRS() ) {
			if ( rs.next() ) {
				boolean exit = false;
				do {
					System.out.println( "current user : " + rs.getInt( "id" ) + " - name " + rs.getString( "name" ) );
					System.out.print( "Entrez une commande : " );
					String command = keyboard.nextLine();
					switch ( command ) {
						case "next":
							rs.next();
							break;
						case "previous":
							rs.previous();
							break;
						case "first":
							rs.first();
							break;
						case "last":
							rs.last();
							break;
						case "lower":
							rs.updateString( "name", rs.getString( "name" ).toLowerCase() );
							rs.updateRow();
							break;
						case "exit":
							exit = true;
							break;
					}
				} while ( !exit );
			}
			
		} catch ( SQLException e ) {
			e.printStackTrace();
		}
	}
	
	public static void main( String[] args ) {
		parcours();
		// createUser();
		iUserDAO dao = DAOFactory.getUserDAO();
		// User user = null;
		// System.out.println( "Bienvenue dans mon APP" );
		// do {
		// 	System.out.print( "Votre login : " );
		// 	String name = keyboard.nextLine();
		// 	System.out.print( "Votre password :" );
		// 	String email = keyboard.nextLine();
		//
		// 	try {
		// 		user = dao.authenticate( name, email );
		// 		if ( user == null ) {
		// 			System.out.println( "Identifiants incorrects" );
		// 		} else {
		// 			System.out.println( "vous êtes connecté : " + user.getName() );
		// 		}
		// 	} catch ( SQLException e ) {
		// 		System.out.println( e.getMessage() );
		// 	}
		// } while ( user == null );
		exit();
	}
}
