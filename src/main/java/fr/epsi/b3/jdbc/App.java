package fr.epsi.b3.jdbc;

import fr.epsi.b3.jdbc.resources.MyResource;

import java.sql.*;
import java.util.ResourceBundle;

public class App {
	
	private static void createUser() {
		
		Connection connection = null;
		try {
			// Class.forName( bundle.getString( "db.url" ) );
			ResourceBundle bundle = ResourceBundle.getBundle( "db" );
			connection = DriverManager
					.getConnection( bundle.getString( "db.url" ), bundle.getString( "db.user" ), bundle
							.getString( "db.password" ) );
			
			Statement st = connection.createStatement();
			int nb = st.executeUpdate( "INSERT INTO USER (name, email) VALUES ('SSY', 'ssy@ssy.org')" );
			
			st.close();
		} catch ( SQLException e ) {
			e.printStackTrace();
		} finally {
			try {
				if ( null != connection && connection.isValid( 2 ) ) {
					connection.close();
				}
			} catch ( SQLException e ) {
				e.printStackTrace();
			}
		}
	}
	
	private static void createUserWithResources() {
		
		ResourceBundle bundle = ResourceBundle.getBundle( "db" );
		try ( Connection connection = DriverManager
				.getConnection( bundle.getString( "db.url" ), bundle.getString( "db.user" ), bundle
						.getString( "db.password" ) ); MyResource r1 = new MyResource( "R1" ); Statement st = connection
				.createStatement(); MyResource r2 = new MyResource( "R2" ) ) {
			int nb = st.executeUpdate( "INSERT INTO USER (name, email) VALUES ('SSY WITH RESOURCES', 'ssy@ssy.org')" );
			System.out.println( nb );
		} catch ( SQLException e ) {
			e.printStackTrace();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		System.out.println( "fin" );
	}
	
	private static void displayUsers() {
		ResourceBundle bundle = ResourceBundle.getBundle( "db" );
		try ( Connection connection = DriverManager
				.getConnection( bundle.getString( "db.url" ), bundle.getString( "db.user" ), bundle
						.getString( "db.password" ) ); Statement st = connection.createStatement(); ResultSet rs = st
				.executeQuery( "SELECT * FROM USER" ) ) {
			ResultSetMetaData rsm = rs.getMetaData();
			
			int nbColumns = rsm.getColumnCount();
			for ( int i = 1; i <= nbColumns; ++i ) {
				System.out.printf( "%30s (%s)", rsm.getColumnName( i ), rsm.getColumnTypeName( i ) );
			}
			System.out.println();
			while ( rs.next() ) {
				for ( int i = 1; i <= nbColumns; ++i ) {
					System.out.printf( "%30s", rs.getString( i ) );
				}
				System.out.println();
			}
			// while(rs.next()) {
			// 	System.out.printf( "%-10d|%-20s|%-20s\n", rs.getInt( 1 ), rs.getString( "name" ), rs.getString( 3 ) );
			// }
			
		} catch ( SQLException e ) {
			e.printStackTrace();
		}
	}
	
	public static void main( String[] args ) {
		displayUsers();
	}
}
