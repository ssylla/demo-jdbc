package fr.epsi.b3.jdbc.dal;

import fr.epsi.b3.jdbc.bo.User;

import java.sql.SQLException;
import java.util.List;

public interface iDAO<T> {
	
	void create( T object ) throws SQLException;
	
	T readById( int id ) throws SQLException;
	
	List<T> readAll();
	
	void update( T object );
	
	void delete( T object );
}
