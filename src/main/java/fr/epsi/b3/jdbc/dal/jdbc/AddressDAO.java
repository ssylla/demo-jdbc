package fr.epsi.b3.jdbc.dal.jdbc;

import fr.epsi.b3.jdbc.bo.Address;
import fr.epsi.b3.jdbc.dal.iDAO;

import java.sql.SQLException;
import java.util.List;

public class AddressDAO implements iDAO<Address> {
	
	@Override
	public void create( Address object ) throws SQLException {
	
	}
	
	@Override
	public Address readById( int id ) throws SQLException {
		return null;
	}
	
	@Override
	public List<Address> readAll() {
		return null;
	}
	
	@Override
	public void update( Address object ) {
	
	}
	
	@Override
	public void delete( Address object ) {
	
	}
}
