package fr.epsi.b3.jdbc.dal;

import fr.epsi.b3.jdbc.bo.Address;

public final class DAOFactory {
	
	private static final String MODE = "JDBC";
	
	private static iUserDAO userDAO;
	private static iDAO<Address> addressDAO;
	
	private DAOFactory() {}
	
	public static iUserDAO getUserDAO() {
		if ( null == userDAO ) {
			switch ( MODE ) {
				case "JPA":
					userDAO = new fr.epsi.b3.jdbc.dal.jpa.UserDAO();
					break;
				default:
					userDAO = new fr.epsi.b3.jdbc.dal.jdbc.UserDAO();
			}
		}
		return userDAO;
	}
	
	public static iDAO<Address> getAddressDAO() {
		if ( null == addressDAO ) {
			switch ( MODE ) {
				case "JPA":
					addressDAO = new fr.epsi.b3.jdbc.dal.jpa.AddressDAO();
					break;
				default:
					addressDAO = new fr.epsi.b3.jdbc.dal.jdbc.AddressDAO();
			}
		}
		return addressDAO;
	}
	
}
