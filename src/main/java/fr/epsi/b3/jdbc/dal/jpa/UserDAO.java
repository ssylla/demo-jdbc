package fr.epsi.b3.jdbc.dal.jpa;

import fr.epsi.b3.jdbc.bo.User;
import fr.epsi.b3.jdbc.dal.iUserDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserDAO implements iUserDAO {
	@Override
	public void create( User user ) throws SQLException {
	
	}
	
	@Override
	public User readById( int id ) throws SQLException {
		return null;
	}
	
	@Override
	public List<User> readAll() {
		return null;
	}
	
	@Override
	public void update( User user ) {
	
	}
	
	@Override
	public void delete( User user ) {
	
	}
	
	@Override
	public User authenticate( String name, String email ) throws SQLException {
		return null;
	}
	
	@Override
	public ResultSet getUsersRS() throws SQLException {
		return null;
	}
}
