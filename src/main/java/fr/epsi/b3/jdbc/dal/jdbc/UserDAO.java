package fr.epsi.b3.jdbc.dal.jdbc;

import fr.epsi.b3.jdbc.bo.User;
import fr.epsi.b3.jdbc.dal.PersistenceManager;
import fr.epsi.b3.jdbc.dal.iUserDAO;

import java.sql.*;
import java.util.List;

public class UserDAO implements iUserDAO {
	
	private static final String CREATE_QUERY_STRING = "INSERT INTO user (name, email) VALUES (?, ?)";
	private static final String READ_QUERY_STRING = "SELECT * FROM user WHERE id = ?";
	private static final String READ_ALL_QUERY_STRING = "SELECT * FROM user";
	private static final String AUTH_QUERY_STRING = "SELECT * FROM user WHERE name = ? AND email = ?";
	
	@Override
	public void create( User user ) throws SQLException {
		Connection connection = PersistenceManager.getConnection();
		try ( PreparedStatement pst = connection.prepareStatement( CREATE_QUERY_STRING, Statement.RETURN_GENERATED_KEYS ) ) {
			pst.setString( 1, user.getName() );
			pst.setString( 2, user.getEmail() );
			pst.executeUpdate();
			try( ResultSet rs = pst.getGeneratedKeys() ) {
				if( rs.next() ) {
					user.setId(rs.getInt( 1 ));
				}
			}
		}
	}
	
	@Override
	public User readById( int id ) throws SQLException {
		// Connection connection = DBConnection.getSingle().getConnection();
		User user = null;
		Connection connection = PersistenceManager.getConnection();
		try ( PreparedStatement pst = connection.prepareStatement( READ_QUERY_STRING ) ) {
			pst.setInt( 1, id );
			try ( ResultSet rs = pst.executeQuery() ) {
				if ( rs.next() ) {
					user = new User( rs.getInt( "id" ), rs.getString( "name" ), rs.getString( "email" ) );
				}
			}
		}
		return user;
	}
	
	@Override
	public List<User> readAll() {
		return null;
	}
	
	@Override
	public void update( User user ) {
	
	}
	
	@Override
	public void delete( User user ) {
	
	}
	
	@Override
	public User authenticate( String name, String email ) throws SQLException {
		User user = null;
		Connection connection = PersistenceManager.getConnection();
		try ( PreparedStatement pst = connection.prepareStatement( AUTH_QUERY_STRING, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE ) ) {
			
			pst.setString( 1, name );
			pst.setString( 2, email );
			
			try ( ResultSet rs = pst.executeQuery() ) {
				if ( rs.next() ) {
					
					user = new User( rs.getInt( "id" ), rs.getString( "name" ), rs.getString( "email" ) );
					rs.updateInt( "nbConnetion", rs.getInt( "nbConnetion" )+1 );
					rs.updateRow();
				}
				System.out.println( pst );
			}
			
		}
		return user;
	}
	
	public ResultSet getUsersRS() throws SQLException {
		
		Connection connection = PersistenceManager.getConnection();
		ResultSet rs;
		try ( Statement st = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE) ) {
			rs = st.executeQuery( READ_ALL_QUERY_STRING );
		}
		return rs;
	}
}
