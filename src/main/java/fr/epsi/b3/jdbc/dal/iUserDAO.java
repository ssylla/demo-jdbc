package fr.epsi.b3.jdbc.dal;

import fr.epsi.b3.jdbc.bo.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface iUserDAO extends iDAO<User> {
	
	User authenticate(String name, String email) throws SQLException;
	
	public ResultSet getUsersRS() throws SQLException;
}
