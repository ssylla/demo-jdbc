package fr.epsi.b3.jdbc.bo;

public class Address {
	
	private int id;
	private String data;
	
	public Address() {	}
	
	public int getId() {
		return id;
	}
	
	public String getData() {
		return data;
	}
}
