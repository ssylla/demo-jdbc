package fr.epsi.b3.jdbc.bo;

import java.io.Serializable;

public class User implements Serializable {
	
	private int id;
	private String name;
	private String email;
	private Address address;
	
	public User() {}
	
	public User( String name, String email ) {
		this.name = name;
		this.email = email;
	}
	
	public User( int id, String name, String email ) {
		this.id = id;
		this.name = name;
		this.email = email;
	}
	
	public void setId( int id ) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "User{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
